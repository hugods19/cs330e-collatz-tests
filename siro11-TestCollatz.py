#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    #corner cases : valid inputs that arent clear
    def test_read_2(self):
        s = "2 2\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  2)
        self.assertEqual(j, 2)

    #fail case
    def test_read_3(self):
        s = "20 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  20)
        self.assertEqual(j, 1)


    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1000, 1000)
        self.assertEqual(v, 112)

    def test_eval_6(self):
        v = collatz_eval(5001, 4000)
        self.assertEqual(v, 215)

    def test_eval_7(self):
        v = collatz_eval(67001,67001)
        self.assertEqual(v, 43)

    def test_eval_8(self):
        v = collatz_eval(3002, 6000)
        self.assertEqual(v, 238)

    def test_eval_9(self):
        v = collatz_eval(5000, 510000)
        self.assertEqual(v, 364)

    def test_eval_10(self):
        v = collatz_eval(40, 19990)
        self.assertEqual(v, 279)

    def test_eval_11(self):
        v = collatz_eval(67000, 65001)
        self.assertEqual(v, 268)

    def test_eval_12(self):
        v = collatz_eval(50051, 5050)
        self.assertEqual(v, 324)
    # -----
    # print
    # -----

    def test_print_1(self):
    	w = StringIO()
    	collatz_print(w, 1, 10, 20)
    	self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
    	w = StringIO()
    	collatz_print(w, 2, 20, 30)
    	self.assertEqual(w.getvalue(), "2 20 30\n")

    def test_print_3(self):
    	w = StringIO()
    	collatz_print(w, 30, 10, 20)
    	self.assertEqual(w.getvalue(), "30 10 20\n")


    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("1 1\n100 100\n210 210\n900 900\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n100 100 26\n210 210 40\n900 900 55\n")

    def test_solve_3(self):
        r = StringIO("10 1\n200 100\n210 201\n1000 9000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 1 20\n200 100 125\n210 201 89\n1000 9000 262\n")

    #test fail case???
    def test_solve_4(self):
        r = StringIO("10 1\n200 100\n210 201\n1000 9000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 1 20\n200 100 125\n210 201 89\n1000 9000 262\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
